#include <stone/stone.hpp>

#include <stdexcept>

namespace stone {
namespace {

std::vector<u64> gen_constants(unsigned n) {
	auto ret = std::vector<u64>{};
	// xorshift* as described by wikipedia:
	auto state = u64{1}; // apparently every non-zero state is fine
	for (auto i = 0u; i < n; ++i) {
		state ^= state >> 12;
		state ^= state << 25;
		state ^= state >> 27;
		ret.push_back(state * 0x2545F4914F6CDD1D);
	}
	return ret;
}

template <u64 I>
constexpr u64 lrot(u64 in) {
	static_assert(0 < I and I < 64);
	return u64{(in << I) bitor (in >> (64u - I))};
}

u64 round(u64 lhs, u64 k0, u64 k1) { return (lrot<53>(lhs) + k0 + lrot<17>(lhs)) xor k1; }

} // anonymous namespace

std::pair<u64, u64> encrypt(u64 lhs, u64 rhs, const expanded_key& key) {
	assert(key.size() > 4u and key.size() % 2 == 0u);
	const auto round_count = (key.size() - 4u) / 2u;
	lhs ^= key[0];
	rhs ^= key[1];

	for (auto i = 0u; i < round_count; ++i) {
		rhs = rhs xor round(lhs, key[2u * i + 2u], key[2u * i + 3u]);
		std::swap(lhs, rhs);
	}

	lhs ^= key[2 * round_count + 2u];
	rhs ^= key[2 * round_count + 3u];
	return {lhs, rhs};
}

std::pair<u64, u64> decrypt(u64 lhs, u64 rhs, const expanded_key& key) {
	assert(key.size() > 4u and key.size() % 2 == 0u);
	const auto round_count = (key.size() - 4u) / 2u;
	lhs ^= key[2 * round_count + 2u];
	rhs ^= key[2 * round_count + 3u];

	for (auto i = round_count; i-- > 0u;) {
		std::swap(lhs, rhs);
		rhs = rhs xor round(lhs, key[2u * i + 2u], key[2u * i + 3u]);
	}

	lhs ^= key[0];
	rhs ^= key[1];
	return {lhs, rhs};
}

expanded_key key_schedule(const std::vector<u64>& key, unsigned rounds) {
	assert(not key.empty());
	assert(rounds > 0u);
	const auto key_size = key.size();
	auto ret = gen_constants(2u * rounds + 4u);
	auto previous = u64{};
	for (auto& k : ret) {
		k ^= previous;
		for (auto i = 0u; i < rounds; ++i) {
			k ^= round(k, key[i % key_size], key[(i + 2u) % key_size]);
		}
		previous = k;
	}
	return ret;
}

} // namespace stone
