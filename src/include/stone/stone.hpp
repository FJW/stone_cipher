#ifndef STONE_STONE_HPP
#define STONE_STONE_HPP

#include <cassert>
#include <cstdint>
#include <tuple>
#include <utility>
#include <vector>

// Security Through Obesity - Nevermind Efficiency
namespace stone {

using u64 = std::uint64_t;
using expanded_key = std::vector<u64>;
using block = std::pair<u64, u64>;

std::pair<u64, u64> encrypt(u64 lhs, u64 rhs, const expanded_key& key);
std::pair<u64, u64> decrypt(u64 lhs, u64 rhs, const expanded_key& key);
expanded_key key_schedule(const std::vector<u64>& key, unsigned rounds);

class stone_cipher {
public:
	stone_cipher(const std::vector<u64>& key, unsigned rounds = 20)
	        : m_key{key_schedule(key, rounds)} {
		assert(rounds % 2 == 0u);
	}

	block encrypt(block b) const { return stone::encrypt(b.first, b.second, m_key); }
	block decrypt(block b) const { return stone::decrypt(b.first, b.second, m_key); }

private:
	expanded_key m_key;
};

} // namespace stone

#endif
