#include <array>
#include <cstdint>
#include <cstring>
#include <iomanip>
#include <iostream>

#include <stone/stone.hpp>

using stone::u64;

int main(int argc, char** argv) {
	auto rounds = 20u;
	auto n = u64{1};
	auto max = 0u;
	if (argc >= 2) {
		rounds = static_cast<unsigned>(std::stoul(argv[1]));
	}
	if (argc >= 3) {
		n = std::stoul(argv[2]);
		if (n == 0) {
			return 1;
		}
	}
	if (argc >= 4) {
		max = static_cast<unsigned>(std::stoul(argv[3]));
	}
	auto ciphers = std::vector<stone::stone_cipher>{};
	for (auto i = 0u; i < n; ++i) {
		ciphers.emplace_back(std::vector<u64>{0, 0, 0, i}, rounds);
	}
	auto buf = std::array<char, sizeof(u64) * 2u>{};
	for (auto i = 0ul; max == 0 or i < max; ++i) {
		for (const auto& cipher : ciphers) {
			const auto [c0, c1] = cipher.encrypt({0, i});
			std::memcpy(buf.data(), &c0, sizeof(c0));
			std::memcpy(buf.data() + sizeof(u64), &c1, sizeof(c1));
			std::cout.write(buf.data(), buf.size());
		}
	}
}
