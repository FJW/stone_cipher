#include <array>
#include <cstdint>
#include <cstring>
#include <iomanip>
#include <iostream>

#include <stone/stone.hpp>

extern "C" {
#include <bbattery.h>
#include <unif01.h>
}

std::pair<std::uint32_t, std::uint32_t> split(stone::u64 n) {
	return {static_cast<std::uint32_t>(n >> 32u), static_cast<std::uint32_t>(n)};
}

template <typename It>
std::vector<stone::u64> read_vec(It begin, It end) {
	auto ret = std::vector<stone::u64>{};
	for (auto it = begin; it != end; ++it) {
		ret.push_back(std::stoull(*it));
	}
	return ret;
}

// The API of TestU01 sucks, apparently we really have to do it this way:
unsigned& get_round_count() {
	static auto round_count = 50u;
	return round_count;
}

std::vector<stone::u64>& get_key_ref() {
	static auto key = std::vector<stone::u64>{0, 0, 0, 0};
	return key;
}

extern "C" {
unsigned stone_gen() {
	static const auto c = stone::stone_cipher(get_key_ref(), get_round_count());
	static auto ctr = stone::u64{};
	static auto out = std::array<std::uint32_t, 4>{{0, 0, 0, 0}};
	static auto used = 4u;

	if (used < 4u) {
		return out[used++];
	}
	const auto [lhs, rhs] = c.encrypt({0, ctr++});
	std::tie(out[0], out[1]) = split(lhs);
	std::tie(out[2], out[3]) = split(rhs);
	used = 1u;
	return out[0];
}
} // extern "C"

int main(int argc, char** argv) {
	auto suite = std::string{"SmallCrush"};
	if (argc >= 2) {
		suite = std::string{argv[1]};
	}
	if (argc >= 3) {
		get_round_count() = static_cast<unsigned>(std::stoull(argv[2]));
	}
	if (argc >= 4) {
		get_key_ref() = read_vec(argv + 3, argv + argc);
	}
	auto gen = unif01_CreateExternGenBits(suite.data(), stone_gen);
	if (suite == "SmallCrush") {
		bbattery_SmallCrush(gen);
	} else if (suite == "Crush") {
		bbattery_Crush(gen);
	} else if (suite == "BigCrush") {
		bbattery_BigCrush(gen);
	} else {
		std::cerr << "Error: unknown suite “" << suite << "”\n";
	}
	unif01_DeleteExternGenBits(gen);
}
