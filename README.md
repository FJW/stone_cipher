The Stone-Cipher
================

STONE is short for “Security Through Obesity - Nevermind Efficiency”, as this was the original idea behind it.
After tweaking here and there, it did however become relatively lightweight at least with low round-counts.
The number of rounds that are needed to make this secure could however very well justify the name.

This leads the the rationale behind Stone: It was not invented with the intention to be secure (though it would
be very cool if it was) or to be used for real (please don't!), but to provide a challenge for beginners that
is not artificially broken in a certain way to demonstrate a certain attack; I *want* this cipher to be secure,
I just wouldn't trust it myself.

At the moment I haven't even looked for any of the common attack-vectors, so it is definitely plausible that
linear or differential cryptanalysis will shred this to pieces. If you take the challenge, you might want to
start there.

Description of the Algorithm
----------------------------


STONE is a blockcipher with 128 bit blocksize and an arbitrary key-length whose only requirement is that it
has to be a multiple of 64 bit. The intended key-size would be 256 bit, but I'll consider attacks on 128 bit
as just as valid, though slightly less impressive.

The overall structure of the algorithm is that of a Feistel-Network with every half having an unsurprising
size of 64 bit. In addition to that, round-keys are xor-ed in before the first and after the last round
(Xor-Encrypt-Xor) as this is a very cheap operation that makes some attacks a little bit harder to mount.

The round-function in STONE is unusual in that it takes a 128 bit round-key (or rather: two 64 bit round-keys)
but only 64 bit as regular input. It is defined as such: (`lrot<N>` refers to a left-rotation by `N` bits)

```cpp
u64 round(u64 lhs, u64 k0, u64 k1) {
	return (lrot<53>(lhs) + k0 + lrot<17>(lhs)) xor k1;
}
```


The key-schedule is a bit more-complicated. First of all it requires a not too small number
of constants, namely 2n+4 64 bit inegers, where n is the number of rounds. These integers
are generated using Xorshift* as described by the German [Wikipedia](https://de.wikipedia.org/wiki/Xorshift#Xorshift*):

```cpp
std::vector<u64> gen_constants(unsigned n) {
	auto ret = std::vector<u64>{};
	auto state = u64{1};
	for (auto i = 0u; i < n; ++i) {
		state ^= state >> 12;
		state ^= state << 25;
		state ^= state >> 27;
		ret.push_back(state * 0x2545F4914F6CDD1D);
	}
	return ret;
}
```

Originally I used, the constants from SHA2 (cube-roots of primes), but switched to Xorshift
to be easily able to generate those inside C++ to be able to deal with different round-counts.
Either way there is no deep meaning behind them, they are just there to give chaos to all
round-keys.

Each of these constants will at first be xor-ed with the previous round-key and then be but
into the round-function together with parts of the key and
xored with the original value as often as the regular cipher has rounds. Since the key will most
likely have fewer 64 bit words in it that there are rounds, the applications will simply cycle through
all key-words:


```cpp
expanded_key key_schedule(const std::vector<u64>& key, unsigned rounds) {
	assert(not key.empty());
        assert(rounds > 0u);
        const auto key_size = key.size();
        auto ret = gen_constants(2u * rounds + 4u);
        auto previous = u64{};
        for (auto& k : ret) {
                k ^= previous;
                for (auto i = 0u; i < rounds; ++i) {
                        k ^= round(k, key[i % key_size], key[(i + 2u) % key_size]);
                }
                previous = k;
        }
        return ret;
}
```

Parameters
----------

The above is naturally a fairly generic description that has many places that allow tweaking, which
is intentional. To provide an interesting challenge, I will however choose some parameters whose breaking
will earn you valuable bounties in the form of my respect and an entry on this side. (I don't
have spare money right now, so that's all there is.)

* 128 bit key, 14 rounds. Generic random-number tests have trouble to get there if fed with the
  output of counter-mode with a bad key.
* 128 bit key, 20 rounds. Probably doable, but probably not without manual analysis.
* 256 bit key, 20 rounds. Likely a bit more challenging than the previous.
* 128 bit key, 30 rounds. This would certainly be impressive.
* 256 bit key, 30 rounds. I would be very surprised if somebody competent enough to find this would
  honor my bountyless crypt-challenge with an analysis.
* 128 bit key, 50 rounds. Full round-count but with smaller key. Given the key-schedule I wouldn't be
  too surprised if a professional cryptanalist would get here. I wouldn't be surprised however if the
  attack is not applicable to the larger key-size.
* 256 bit key, 50 rounds. I'll consider this the full-sized version. If you manage to get here, the cipher
  is probably so flawed that additional rounds won't fix it.

Hall of Fame
------------


Empty so far.



License
-------

Everything in this repo is Public Domain/CC-0.

